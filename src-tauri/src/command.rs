use tauri::{Manager, Wry};

/// 当页面加载好后，显示当前窗口
#[tauri::command]
pub fn ready(app_handle: tauri::AppHandle<Wry>){
    let main_window = app_handle.get_window("main").unwrap();
    if !main_window.is_visible().unwrap() {
        main_window.show().unwrap();
    }
}

/// 显示/隐藏底板（也称指针提示浮标窗口）
#[tauri::command]
pub fn is_show_buoy(app_handle: tauri::AppHandle<Wry>, target: bool){
    let buoy_window = app_handle.get_window("buoy").unwrap();

    // 如果传进来的为true则说明要显示底板，反之隐藏
    if target {
        buoy_window.show().unwrap();
    }else {
        buoy_window.hide().unwrap();
    }
}

/// 退出程序
#[tauri::command]
pub fn exit(){
    std::process::exit(0);
}
