use tauri::{App, AppHandle, GlobalWindowEvent, Manager, PhysicalPosition, Wry};
use crate::mouse;

/// 负责 setup 里面具体逻辑工作，简化 main 代码
pub fn setup(app: &mut App<Wry>){
    let main_window = app.handle().get_window("main").unwrap();
    // 设置窗口向上偏移
    main_window.set_position(PhysicalPosition::new(
        main_window.inner_position().unwrap().x,
        main_window.inner_position().unwrap().y - 150
    )).unwrap();

    // 底板（也称指针提示浮标窗口）
    let buoy = app.handle().get_window("buoy").unwrap();
    buoy.set_ignore_cursor_events(true).unwrap();  // 忽略鼠标监听事件

    // 开启鼠标监听
    mouse::init(app);
}

/// 单实例
pub fn single_instance(app:&AppHandle){
    let window = app.get_window("main").unwrap();
    window.show().unwrap();
    //窗体最小化后显示窗体
    window.unminimize().unwrap();
    //使窗体获取焦点，显示在最顶部
    window.set_focus().unwrap();
}

/// 窗口事件
pub fn window_event(event: GlobalWindowEvent<Wry>){
    match event.event() {
        tauri::WindowEvent::CloseRequested { api, .. } => {
            // 隐藏窗口
            event.window().hide().unwrap();
            // 防止窗口关闭
            api.prevent_close();
        }
        _ => {}
    }
}
