use tauri::{CustomMenuItem, Manager, SystemTray, SystemTrayEvent, SystemTrayMenu};

/// 提供一个完整的系统托盘
pub fn get_tray() -> SystemTray{
    let tray_menu = create_tray_menu();
    SystemTray::new()
        .with_tooltip("mouse-buoy")
        .with_menu(tray_menu)
}

/// 创建托盘菜单
fn create_tray_menu() -> SystemTrayMenu{
    SystemTrayMenu::new()
        .add_item(CustomMenuItem::new("exit".to_string(),"退出（Exit）"))
}

/// 提供系统托盘的监听事件
pub fn get_tray_event(app: &tauri::AppHandle, event: SystemTrayEvent){
    let main_window = app.get_window("main").unwrap();

    match event {
        // 监听左键点击
        SystemTrayEvent::LeftClick {..} => {
            if !main_window.is_visible().unwrap() {
                main_window.show().unwrap();
            }
            main_window.unminimize().unwrap();
            main_window.set_focus().unwrap();
        }

        // 监听托盘菜单项事件单击
        SystemTrayEvent::MenuItemClick { id, .. } => {
            // 以项的 id 做判断
            match id.as_str() {
                "exit" => {
                    std::process::exit(0);
                }
                _ => {}
            }
        }
        _ => {}
    }
}
