use device_query::{DeviceEvents, DeviceState};
use tauri::{App, Manager, PhysicalPosition, Wry};

/// 鼠标监听初始化
pub fn init(app: &mut App<Wry>){
    mouse_move_event(app);
    mouse_down_event(app);
    mouse_up_event(app);
}

/// 鼠标移动监听
fn mouse_move_event(app: &mut App<Wry>){
    let buoy = app.handle().get_window("buoy").unwrap();
    app.manage(
        DeviceState::new().on_mouse_move(move |mouse_position| {
            // 获取缩放因子以计算缩放比下的窗口中心
            let scale_factor = buoy.scale_factor().unwrap();

            // 因不同缩放比下窗口实际大小不同，故此处计算需要考虑到缩放比
            let position = PhysicalPosition::new(
                mouse_position.0 - ((150.0*scale_factor)/2.0) as i32,
                mouse_position.1 - ((150.0*scale_factor)/2.0) as i32
            );

            // 设置窗口位置
            buoy.set_position(position).unwrap();
        })
    );
}

/// 鼠标按下监听
fn mouse_down_event(app: &mut App<Wry>){
    let buoy = app.handle().get_window("buoy").unwrap();
    app.manage(
        DeviceState::new().on_mouse_down(move |button| {
            match button {
                // 当按下左键
                1 => {
                    buoy.emit(
                        "left",
                        "true"
                    ).unwrap();
                }

                // 右键
                2 => {
                    buoy.emit(
                        "right",
                        "true"
                    ).unwrap();
                }
                &_ => {}
            }
        })
    );
}

/// 鼠标抬起监听
fn mouse_up_event(app: &mut App<Wry>){
    let buoy = app.handle().get_window("buoy").unwrap();
    app.manage(
        DeviceState::new().on_mouse_up(move |button| {
            match button {
                1 => {
                    buoy.emit(
                        "left",
                        "false"
                    ).unwrap();
                }

                2 => {
                    buoy.emit(
                        "right",
                        "false"
                    ).unwrap();
                }
                &_ => {}
            }
        })
    );
}
