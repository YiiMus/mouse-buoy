// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use tauri_plugin_autostart::MacosLauncher;

// 负责托盘相关工作模块
mod tray;
// 负责命令相关工作模块
mod command;
// 负责鼠标相关工作模块
mod mouse;
// 负责初始化相关工作模块
mod init;

fn main() {
    tauri::Builder::default()
        .setup(|app| { init::setup(app); Ok(()) })
        .plugin(tauri_plugin_single_instance::init(|app,_,_| init::single_instance(app)))
        .plugin(tauri_plugin_autostart::init(MacosLauncher::LaunchAgent, None))
        .plugin(tauri_plugin_store::Builder::default().build())
        .system_tray(tray::get_tray())
        .on_system_tray_event(tray::get_tray_event)
        .invoke_handler(tauri::generate_handler![
            // 注册命令
            command::ready,
            command::is_show_buoy,
            command::exit
        ])
        .on_window_event(|event| init::window_event(event))
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
