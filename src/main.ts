import { createApp } from "vue";
import "./styles.css";
import App from "./App.vue";
//引入vue-router
import router from "./router";

createApp(App)
    .use(router)
    .mount("#app");
