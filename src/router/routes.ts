import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: ()=> import("../pages/Main.vue")
    },
    {
        path: '/buoy',
        component: ()=> import("../pages/Buoy.vue")
    },
    {
        path: '/setting',
        component: ()=> import("../pages/Setting.vue")
    }
];

export { routes }